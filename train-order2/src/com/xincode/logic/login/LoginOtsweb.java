package com.xincode.logic.login;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.xincode.constant.LoginConstants;
import com.xincode.constant.UrlConstants;
import com.xincode.domain.Passenger;
import com.xincode.domain.User;
import com.xincode.utils.HttpUtils;
import com.xincode.utils.JavaUtils;
import com.xincode.utils.RandCodeUtils;


public class LoginOtsweb {
	
	//登录参数Map
	public Map<String,String> loginPostParams = new LinkedHashMap<String,String>();
		
	//定义一个数组，用来存储用户登录信息
	public String[] loginParamValues = new String[9];
	
	/**
	 * 根据用户名密码返回登录请求POST的参数
	 * @param user
	 * @return 
	 * @throws IOException
	 */
	public String getLoginPostParam(User user) throws IOException
	{
		loginParamValues[0] = getLoginRand();
		loginParamValues[1] = user.getUsername();
	    loginParamValues[2] = "";
		loginParamValues[3] = "";
		loginParamValues[4] = RandCodeUtils.getLoginRandCode(UrlConstants.GET_RANDCODE_LOGIN_URL, UrlConstants.SAVELOGINCODEPATH);
		loginParamValues[5] = "focus";
		loginParamValues[6] = "Y";
		loginParamValues[7] = "N";
		loginParamValues[8] = user.getPassword();
		for (int i = 0; i < loginParamValues.length; i++) {
			loginPostParams.put(LoginConstants.loginParamNames[i],loginParamValues[i] );
		}
		return JavaUtils.toPostParam(loginPostParams);
	}
	
	/**
	 * 登录前获取loginRand
	 * @return
	 */
	public String getLoginRand()
	{
		String loginAysnSuggest = HttpUtils.doGetBody(UrlConstants.LOGINAYSNSUGGEST_URL, null);
		return loginAysnSuggest.substring(loginAysnSuggest.indexOf("loginRand\":\"")+"loginRand\":\"".length(), loginAysnSuggest.indexOf("\",\""));
	}
	
	/**
	 * 登录
	 * @param user
	 * @param cookieStr
	 * @param redirt
	 * @throws IOException
	 */
	public void login(User user,String cookieStr,boolean redirt) throws IOException
	{
		String body = HttpUtils.doPostBody(UrlConstants.LOGIN_URL, getLoginPostParam(user), cookieStr, "UTF-8", redirt);
		if (body.indexOf(user.getDisplayname()) != -1) {
			System.out.println(user.getDisplayname()+",恭喜您登录成功！");
		}
	}
	
}
