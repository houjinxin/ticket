package com.xincode.email;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * @author houjinxin
 * 邮件用户名密码验证
 */
public class MailAuthenticator extends Authenticator {
	
	private String username;
	private String password;
	
	public MailAuthenticator(String username, String password) {    
	    this.username = username;    
	    this.password = password;    
	}
	
	protected PasswordAuthentication getPasswordAuthentication(){
		return new PasswordAuthentication(username, password);
	}
}
