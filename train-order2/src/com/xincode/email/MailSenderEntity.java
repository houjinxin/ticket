package com.xincode.email;

import java.util.Properties;

/**
 * @author houjinxin
 * 发送邮件用到的基本信息
 */

public class MailSenderEntity {

	//发送邮件的服务器的IP和端口
	private String mailServerHost;
	private String mailServerPort;
	
	//邮件发送者的地址
	private String fromAddress;
	
	//邮件接收者的地址
	private String toAddress;
	
	//登录邮件发送服务器的用户名和密码
	private String username;
	private String password;
	
	//是否需要身份验证
	private boolean validate =false;
	
	//邮件主题
	private String subject;
	
	//邮件文本内容
	private String content;
	
	//邮件附件的文件名
	private String[] attachFileNames;
	
	/**
	 * 获得邮件的会话属性
	 */
	public Properties getProperties()
	{
		Properties properties = new Properties();
		properties.put("mail.smtp.host", this.mailServerHost);
		properties.put("mail.smtp.port", this.mailServerPort);
		properties.put("mail.smtp.auth", validate?"true":"false");
		properties.put("mail.smtp.timeout", 25000);
		//这几个参数不明白
		properties.put("mail.smtp.starttls.enable", true);
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.fallback", false);
		return properties;
	}

	public String getMailServerHost() {
		return mailServerHost;
	}

	public void setMailServerHost(String mailServerHost) {
		this.mailServerHost = mailServerHost;
	}

	public String getMailServerPort() {
		return mailServerPort;
	}

	public void setMailServerPort(String mailServerPort) {
		this.mailServerPort = mailServerPort;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String[] getAttachFileNames() {
		return attachFileNames;
	}

	public void setAttachFileNames(String[] attachFileNames) {
		this.attachFileNames = attachFileNames;
	}
	
}
