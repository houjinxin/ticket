package com.xincode.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *  日期处理类
 *  @author houjinxin
 */
public class DateUtils {
	
	/**
	 * 格式化日期，将日期为'YYYY-MM-DD'格式
	 * @param aMask
	 * @param aDate
	 * @return
	 */
	public static final String getDateTime(String aMask, Date aDate) {
		if (aDate == null) {
			return "";
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat(aMask);
			return dateFormat.format(aDate);
		}
	}
	
	/**
	 * 根据预售天数返回发车日期
	 * @param dayBeforeTrainDateTime
	 * @return
	 */
	public static final Date getTrainDateTime(int dayBeforeTrainDateTime)
	{
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_YEAR);
		calendar.set(Calendar.DAY_OF_YEAR, day+dayBeforeTrainDateTime-1);
		return calendar.getTime();
	}
}
