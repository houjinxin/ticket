package com.xincode.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;

import com.xincode.constant.UrlConstants;

public class RandCodeUtils {

	
	/**
	 * 获取验证码
	 * @return
	 * @throws IOException
	 */
	public static String getLoginRandCode(String randcodeUrl,String randcodeSavepath) throws IOException{
		File file = HttpUtils.doGetFile(randcodeUrl, null);
		File codeFile = new File(randcodeSavepath);
		if(!codeFile.exists())
			codeFile.createNewFile();
		FileUtils.copyFile(file, codeFile);
		System.out.println("验证码路径：["+ UrlConstants.SAVELOGINCODEPATH +"]");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		try{
			System.out.print("请输入验证码: ");
			return bufferedReader.readLine();
		}catch(Exception e){
		}
		return "1245";
	}
}
