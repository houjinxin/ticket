package com.xincode.constant;

import java.util.LinkedHashMap;
import java.util.Map;

public class UrlConstants {
	
	//登录验证码保存位置
	public static final String SAVELOGINCODEPATH = "D://login-code.jpg";
	//订单验证码保存位置
	public static final String SAVESUMBITCODEPATH = "D://submit-code.jpg";
	//获取登陆验证码URL
	public static final String GET_RANDCODE_LOGIN_URL= "https://dynamic.12306.cn/otsweb/passCodeAction.do?rand=sjrand";
	//获取提交订单验证码URL
	public static final String GET_RANDCODE_PASSENGER_URL= "https://dynamic.12306.cn/otsweb/passCodeAction.do?rand=randp";
	//获取loginRand的URL
	public static final String LOGINAYSNSUGGEST_URL = "https://dynamic.12306.cn/otsweb/loginAction.do?method=loginAysnSuggest";
	//登录URL
	public static final String LOGIN_URL = "https://dynamic.12306.cn/otsweb/loginAction.do?method=login";
	//登录初始化URL
	public static final String LOGIN_INIT_URL = "https://dynamic.12306.cn/otsweb/order/querySingleAction.do?method=init";
	//查询余票URL
	public static final String QUERYLEFTTICKET_URL = "https://dynamic.12306.cn/otsweb/order/querySingleAction.do?method=queryLeftTicket";
	//预定URL
	public static final String SUBMUTORDERREQUEST_URL = "https://dynamic.12306.cn/otsweb/order/querySingleAction.do?method=submutOrderRequest";
	//提交订单前检查信息URL
	public static final String CHECKORDERINFO_URL = "https://dynamic.12306.cn/otsweb/order/confirmPassengerAction.do?method=checkOrderInfo";
	//点击确认按钮返回订单队列中顺序URL
	public static final String GETQUEUECOUNT_URL = "https://dynamic.12306.cn/otsweb/order/confirmPassengerAction.do?method=getQueueCount";
	//进入订单队列
	public static final String CONFIRMSINGLEFORQUEUE_URL = "https://dynamic.12306.cn/otsweb/order/confirmPassengerAction.do?method=confirmSingleForQueue";
	
}
