package com.xincode.constant;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TrainConstants {
	
	//席别在提交数据中的可选值
	public static Map<String,String> seatNameValueMap = new LinkedHashMap<String,String>();
	//查询列车信息得到的结果对应表中的列索引
	// 0     1     2     3      4        5         6        7         8            9     10    11   12    13    14   15
	//车次|发站|到站|历时|商务座|特等座|一等座|二等座|高级软卧|软卧|硬卧|软座|硬座|无座|其他|预定
	public static Map<String,Integer> seatNameIndexMap = new LinkedHashMap<String,Integer>();

	static{
		seatNameValueMap.put("商务座","9");
		seatNameIndexMap.put("商务座", 5);
		
		seatNameValueMap.put("特等座", "P");
		seatNameIndexMap.put("特等座", 6);
		
		seatNameValueMap.put("一等座", "M");
		seatNameIndexMap.put("一等座", 7);
		
		seatNameValueMap.put("二等座","O");
		seatNameIndexMap.put("二等座", 8);
		
		seatNameValueMap.put("高级软卧", "6");
		seatNameIndexMap.put("高级软卧", 9);
		
		seatNameValueMap.put("软卧", "4");
		seatNameIndexMap.put("软卧", 10);
		
		seatNameValueMap.put("硬卧", "3");
		seatNameIndexMap.put("硬卧", 11);
		
		seatNameValueMap.put("软座", "2");
		seatNameIndexMap.put("软座", 12);
		
		seatNameValueMap.put("硬座", "1");
		seatNameIndexMap.put("硬座", 13);
	}

	//部分订单参数名
	public static final String[] orderParamPartNames = {
		"station_train_code","lishi","train_start_time",
		"trainno4","from_station_telecode","to_station_telecode",
		"arrive_time","from_station_name","to_station_name",
		"from_station_no","to_station_no","ypInfoDetail",
		"mmStr","locationCode"
	};
	
	/**
	 * 请求订单参数 目前为止共25个
	 */
	public static Map<String,String> queryOrderParams = new HashMap<String, String>();
	static{
		queryOrderParams.put("round_start_time_str", "00:00--24:00");
		queryOrderParams.put("start_time_str", "00:00--24:00");
		queryOrderParams.put("train_class_arr", "QB#D#Z#T#K#QT#");
		queryOrderParams.put("train_pass_type", "QB");
		queryOrderParams.put("single_round_type", "1");
		queryOrderParams.put("include_student", "00");
		queryOrderParams.put("seattype_num", "");
	}
}
